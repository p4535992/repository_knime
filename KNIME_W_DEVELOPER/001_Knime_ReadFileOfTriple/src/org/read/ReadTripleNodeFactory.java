package org.read;

import org.knime.core.node.NodeDialogPane;
import org.knime.core.node.NodeFactory;
import org.knime.core.node.NodeView;

/**
 * <code>NodeFactory</code> for the "ReadTriple" Node.
 * 
 *
 * @author 
 */
public class ReadTripleNodeFactory 
        extends NodeFactory<ReadTripleNodeModel> {

    /**
     * {@inheritDoc}
     */
    @Override
    public ReadTripleNodeModel createNodeModel() {
        return new ReadTripleNodeModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getNrNodeViews() {
        return 1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeView<ReadTripleNodeModel> createNodeView(final int viewIndex,
            final ReadTripleNodeModel nodeModel) {
        return new ReadTripleNodeView(nodeModel);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasDialog() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeDialogPane createNodeDialogPane() {
        return new ReadTripleNodeDialog();
    }

}

